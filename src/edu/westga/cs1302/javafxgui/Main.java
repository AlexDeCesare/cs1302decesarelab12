package edu.westga.cs1302.javafxgui;
	
import edu.westga.cs1302.javafxgui.view.EventPane;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;

/**
 * The main method for the stages 
 * 
 * @author Alex DeCesare
 * @version 09-July-2020
 */

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {

		primaryStage.setTitle("Alex DeCesare's Lab 12");
		
		EventPane root = new EventPane();
		
		Scene rootScene = new Scene(root);
		
		primaryStage.setScene(rootScene);
		
		primaryStage.show();
		
	}
	
	/**
	 * Launches the application
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param args the arguments
	 */
	
	public static void main(String[] args) {
		launch(args);
	}
}
