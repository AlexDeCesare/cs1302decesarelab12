package edu.westga.cs1302.javafxgui.view;

import edu.westga.cs1302.javafxgui.model.NumberMath;

/**
 * The view class for the application
 * 
 * @author Alex DeCesare
 * @version 09-July-2020
 */

public class View {
	
	/**
	 * The constructor for the view
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	
	public View() {
		
	}
	
	/**
	 * Gets the value of the two inputted numbers added together
	 * 
	 * @precondition firstNumber != null && firstNumber.isEmpty == false secondNumber != null && secondNumber.isEmpty == false
	 * @postcondition parsedFirstNumber == firstNumber && parsedSecondNumber == secondNumber
	 * 
	 * @param firstNumber the first number to add
	 * @param secondNumber the second number to add
	 * 
	 * @return a string describing that the numbers were added together and its result
	 */
	
	public String formatAddNumbers(String firstNumber, String secondNumber) {
		
		int parsedFirstNumber;
		int parsedSecondNumber;
		
		try {
			
			if (firstNumber == null) {
				throw new IllegalArgumentException("The first number cannot be null");
			}
			if (firstNumber.isEmpty()) {
				throw new IllegalArgumentException("Please enter a value for the first number");
			}
			if (secondNumber == null) {
				throw new IllegalArgumentException("The second number cannot be null");
			}
			if (secondNumber.isEmpty()) {
				throw new IllegalArgumentException("Please enter a value for the second number");
			}
			
			parsedFirstNumber = Integer.parseInt(firstNumber);
			parsedSecondNumber = Integer.parseInt(secondNumber);
			
		} catch (NumberFormatException theNumberFormatException) {
			return "Please only input numbers into the input boxes";
		}
		
		NumberMath theNumberMath = new NumberMath(parsedFirstNumber, parsedSecondNumber);
		
		return "The sum of the numbers is: " + theNumberMath.addNumbers();
		
	}

}
