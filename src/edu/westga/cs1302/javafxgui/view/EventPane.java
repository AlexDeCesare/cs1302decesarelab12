package edu.westga.cs1302.javafxgui.view;

import edu.westga.cs1302.javafxgui.controllers.GuiController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

/**
 * The pane for the event GUI
 * 
 * @author Alex DeCesare
 * @version 09-July-2020
 */

public class EventPane extends GridPane {

	private TextField addFirstNumberTextField;
	private TextField addSecondNumberTextField;
	private Label outputLabel;
	private Button addNumbersButton;
	private Button exitButton;
	
	/**
	 * The constructor for the Event Pane
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	
	public EventPane() {
		
		HandleAddNumbers addNumbers = new HandleAddNumbers();
		HandleExit exitProgram = new HandleExit();
		
		Text titleText = new Text("Add two numbers together");
		super.add(titleText, 0, 0);
		
		Label addFirstNumberLabel = new Label("First Number to Add: ");
		super.add(addFirstNumberLabel, 0, 1);
		
		this.addFirstNumberTextField = new TextField();
		super.add(this.addFirstNumberTextField, 1, 1);
		
		Label addSecondNumberLabel = new Label("Second Number to Add: ");
		super.add(addSecondNumberLabel, 0, 2);
		
		this.addSecondNumberTextField = new TextField();
		super.add(this.addSecondNumberTextField, 1, 2);
		
		this.addNumbersButton = new Button("Add Numbers");
		this.addNumbersButton.setOnAction(addNumbers);
		super.add(this.addNumbersButton, 0, 3);
		
		this.exitButton = new Button("Exit");
		this.exitButton.setOnAction(exitProgram);
		super.add(this.exitButton, 1, 3);
		
		this.outputLabel = new Label("");
		super.add(this.outputLabel, 0, 5, 1, 1);
		
		this.styleTheGui();
	}
	
	/**
	 * The getter for the first number input
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the first number input
	 */
	
	public String getFirstNumberInput() {
		return this.addFirstNumberTextField.getText();
	}
	
	/**
	 * The getter for the second number input
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the second number input
	 */
	
	public String getSecondNumberInput() {
		return this.addSecondNumberTextField.getText();
	}
	
	/**
	 * The setter for the output
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param output the output to set the output label to
	 */
	
	public void setOutput(String output) {
		this.outputLabel.setText(output);
	}
	
	private class HandleAddNumbers implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent arg0) {
			
			GuiController theController = new GuiController();
			
			String theOutput = theController.addNumbers(EventPane.this.getFirstNumberInput(), EventPane.this.getSecondNumberInput());
			
			EventPane.this.setOutput(theOutput);
		}
	}
	
	private class HandleExit implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent arg0) {
			System.exit(0);
		}
		
	}
	
	private void styleTheGui() {
		super.setPrefSize(500, 700);
		super.setAlignment(Pos.CENTER);
		super.setVgap(20);
		super.setHgap(100);
		this.outputLabel.setPrefWidth(200);
		this.outputLabel.setWrapText(true);
		this.addNumbersButton.setPrefWidth(120);
		this.exitButton.setPrefWidth(150);
	}

}
