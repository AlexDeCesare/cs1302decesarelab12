package edu.westga.cs1302.javafxgui.controllers;

import edu.westga.cs1302.javafxgui.view.View;

/**
 * This is the controller for the Javafx Gui
 * 
 * @author Alex DeCesare
 * @version 10-July-2020
 */

public class GuiController {
	
	private View theView;
	
	/**
	 * The constructor for the Gui Controller
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	
	public GuiController() {
		this.theView = new View();
	}
	
	/**
	 * Adds two numbers together
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param firstNumber the first number to add
	 * @param secondNumber the second number to add
	 * 
	 * @return a string describing the result of the two numbers
	 */
	
	public String addNumbers(String firstNumber, String secondNumber) {
		
		try {
			return this.theView.formatAddNumbers(firstNumber, secondNumber);
		} catch (IllegalArgumentException theIllegalArgumentException) {
			return theIllegalArgumentException.getMessage();
		}
		
	}
	
}
